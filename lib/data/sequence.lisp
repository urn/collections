"Manipulation of lazy sequences of data.
 These are implemented as linked lists where the [[cdr]] is a thunk."

(import lazy (thunk force))
(import lua/basic (setmetatable getmetatable))
(import base b)

(define sequence-mt :hidden
  "The metatable for sequences."
  (struct :--pretty-print seq-pp))

(defun car (x)
  "Return the first element of the sequence X.

   Example:
   ```cl
   > (seq/car (seq/of '(1 2 3 4 5)))
   out = 1
   ```"
  (.> x :car))

(defun cdr (x)
  "Force the remaining elements of the sequence X, with the first
   element removed.

   Example:
   ```cl
   > (seq/cdr (seq/of '(1 2 3 4 5)))
   out = (seq/cons 2 <<thunk>>)
   ```"
  (force (.> x :cdr)))

(define null
  "The nil sequence. Well-formed sequences will have
   [[null]] as the last element."
  (setmetatable
    (struct :tag "sequence"
            :nil true
            :car nil
            :cdr nil)
    sequence-mt))

(defmacro cons (x y)
  "Add the element X the front of the (delayed) sequence Y.

   Example:
   ```cl
   > (seq/cons 1 2)
   out = (seq/cons 1 <<thunk>>)
   ```"
  `(setmetatable
     (struct :tag "sequence"
             :car ,x
             :cdr (thunk ,y))
     sequence-mt))

(defun seq-pp (x) :hidden
  (if (.> x :nil)
    "seq/seq-nil"
    (let* [(hd (.> x :car))
           (tl (.> x :cdr))]
      (.. "(seq/cons "
          (pretty hd) " "
          (pretty tl)
          ")"))))

(defun map (f x)
  "Execute the function F over all elements of the sequence X.

   Example:
   ```cl
   > (seq/to-list (seq/map succ (seq/range 1 10)))
   out = (2 3 4 5 6 7 8 9 10 11)
   ```"
  (if (.> x :nil)
    null
    (cons (f (car x)) (map f (cdr x)))))

(defun of (ls)
  "Build a sequence based on the list LS.

   Example: 
   ```cl
   > (seq/of '(1 2 3 4))
   out = (seq/cons 1 <<thunk>>)
   > (cdr (seq/of '(1 2 3 4)))
   out = (seq/cons 2 <<thunk>>)
   ``` "
  (if (= (# ls) 0)
    null
    (cons (b/car ls) (of (b/cdr ls)))))

(defun foldr (f z xs)
  "Accumulate the list XS using the binary function F and the zero element Z.
   This function is also called `reduce` by some authors. One can visualise
   (foldr f z xs) as replacing the [[cons]] operator in building lists with F,
   and the empty sequence with Z.

   Example:
   ```cl
   > (seq/foldr + 0 (seq/range 1 10))
   out = 55
   ```"
  (if (.> xs :nil)
    z
    (f (.> xs :car) (foldr f z (force (.> xs :cdr))))))

(defun take (cl n)
  "Keep the first N elements of the sequence CL, discarding the rest.

   Example:
   ```cl
   > (seq/to-list (seq/take (seq/range 1 10) 5))
   out = (1 2 3 4 5)
   ```"
  (if (or (! n) (= n 0))
    null
    (cons (car cl) (take (cdr cl) (- n 1)))))

(defun drop (cl n)
  "Remove the first N elements of the collection CL.

   Example:
   ```cl
   > (seq/drop (seq/range 1 10) 5)
   out = (seq/cons 6 <<thunk>>)
   ```"
  (if (= n 0)
    cl
    (drop (cdr cl) (- n 1))))

(defun to-list (cl)
  "Convert the sequence CL to a list, forcing all elements
   in the process.

   Example:
   ```
   > (seq/to-list (seq/of '(1 2 3)))
   out = (1 2 3)
   ```"
  (foldr b/cons '() cl))

(defun range (x y)
  "Build a lazy sequence from the integer value X to the integer
   value Y.

   Example:
   ```cl
   > (seq/range 1 10)
   out = (seq/cons 1 <<thunk>>)"
  (if (> x y)
    null
    (cons x (range (+ x 1) y))))
