"Utilities for dealing with lazy data."

(defun map (f thnk)
  (thunk (f (force thnk))))

(defmacro thunk (x)
  "Return a suspended form of the computation X.

   Thunks have the following properties:
   - A thunk for X will evaluate X at most once, and cache
     the result for consecutive applications of [[force]].
   - Pretty-printing a thunk does not cause an application
     of [[force]].
   - Thunks may be mapped over: The result is a thunk of
     applying the given function to forcing the \"outer\" thunk.  "
  (let* [(pp-param (gensym))]
    `(setmetatable
       (struct :tag "thunk"
               :val (lambda () ,x)
               :eval false)
       (struct :--pretty-print
               (lambda (,pp-param)
                 (if (.> ,pp-param :eval)
                   (pretty (.> ,pp-param :val))
                   "<<thunk>>"))
               ))))

(defun force (thnk)
  (if (= (type thnk) "thunk")
    (if (.> thnk :eval)
      (.> thnk :val)
      (progn
        (setf! (.> thnk :val) ((.> thnk :val)))
        (setf! (.> thnk :eval) true)
        (.> thnk :val)))
    thnk))
