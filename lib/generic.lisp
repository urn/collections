(defmacro defgeneric (name ll &description)
  "Define a generic method NAME with the arguments specified in LL."
  (let* [(lookup-table-sym (sym.. name '.lookup))
         (kwargs (apply struct description))
         (lookup-table-decl
           `(define ,lookup-table-sym ,@(if (.> kwargs :hide)
                                          '(:hidden)
                                          '())
              {}))
         (arg-syms (map gensym ll))
         (lookup-body
           (let* [(types-sym (gensym))
                  (param-name (gensym))]
             (if (.> kwargs :lookup)
               `(,(.> kwargs :lookup) ,@arg-syms)
               `(let* [(,types-sym (map (lambda (,param-name)
                                          { :tag "symbol"
                                            :contents (type ,param-name) })
                                        (list ,@arg-syms)))]
                  (cond
                    [(.> ,lookup-table-sym (pretty ,types-sym))
                     ((.> ,lookup-table-sym (pretty ,types-sym))
                      ,@arg-syms)]
                    [(.> ,lookup-table-sym :default)
                     ((.> ,lookup-table-sym :default)
                      ,@arg-syms)]
                    (error! (.. ,(.. "no such method ("
                                     (pretty name)
                                     " ")
                                (concat ,types-sym " ")
                                ")")))))))
         (lookup-fn
           `(define ,name ,@(if (.> kwargs :hide)
                              '(:hidden)
                              '())
              (lambda ,arg-syms
                ,lookup-body)))]
    (unpack (list lookup-table-decl lookup-fn))))

(defmacro defmethod (name args &body)
  "Add BODY as a case to the generic method NAME when the types of
   arguments match those in ARGS."
  (let* [(def (eq? (car body) ':default))
         (args-with-types (if (! def)
                            (progn
                              (when (/= (% (# args) 2) 0)
                                (error! (.. "there needs to be an even number of arguments\n"
                                            "got " (# args) " instead")))
                              (let* [(out {})]
                                (for i 1 (# args) 2
                                     (set-idx! out (nth args (succ i)) (nth args i)))
                                out))
                            (progn
                              (let* [(out {})]
                                (for i 1 (# args) 1
                                  (set-idx! out (nth args i) 'nil))
                                out))))
         (body (if def (cdr body) body))]
    `(set-idx! ,(sym.. name '.lookup)
               ,(if def
                  "default"
                  (pretty (let* [(out '())]
                          (iter-pairs args-with-types (lambda (_ t)
                                                        (push-cdr! out t)))
                          out)))
               (lambda ,(keys args-with-types)
                 ,@body))))
