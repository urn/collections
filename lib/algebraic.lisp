"Algebraic data type support for Urn.

 Algebraic data types are so called because they are formed by two basic
 operators, the sum (`+`) and product (`*`). A sum type encodes logical
 OR over types, that is, `A + B` means `A or B`, while a product type 
 encodes logical AND over type, i.e. `A * B` is `A and B`.

 If you have a value of type `A` or a value of type `B`, you can _lift_
 them into a sum `A + B` using one of two injections: `in-l`, for lifting
 `A` into `A + B`, or `in-r`, for lifting `B` into `A + B`.

 If you have a value of type `A * B`, you can _extract_ either a value of
 type `A` or a value of type `B` using the projections `proj-l` or `proj-r`,
 commonly known as `fst`/`snd` or `car`/`cdr`.

 All data types may be encoded using only sums of products, assuming they
 are named (nameless recursion is not a factor here): For example, lists
 can be described as _either the empty list or a pair between some value
 and another list_. Algebraically speaking, that's `() + (a * (list a))`.

 This library implements algebraic data types with named injections
 (constructors), and optionally with named projections (record selectors).

 If several injections have an overlapping projection, it'll match all of
 them. Considering injections are lightweight wrappers around lists, they
 can be deconstructed using regular [[case]]/[[destructuring-bind]] style
 pattern matching."

(import lens ())
(import string str)
(import string (char-at))

(defmacro defalgebraic (name_ &injections)
  "Define an algebraic data type called NAME with the given INJECTIONS.
   NAME may be either a symbol or a list, if the type is parametric, and
   injections may be either a symbol, if it has no arguments, or may
   be a list with a name and parameters. The parameters to an injection
   _must_ be either an invocation of the NAME, or present in the NAME."

  (let* [(name nil)
         (params '())]
    (case name_
      [(?nm . ?pr)
       (set! name nm)
       (set! params pr)]
      [?x (set! name x)])
    (letrec [(valid-param? (x)
               (case x
                 [(?nm . ?prms)
                  (all valid-param? prms)]
                 [?x (let* [(xstr (symbol->string x))]
                       (if (= (str/lower (char-at xstr 1)) (char-at xstr 1))
                         (elem? x params)
                         true))]))
             (is-recursive? (x)
               (case x
                 [(?nm . ?prms)
                  (and (eq? nm name)
                       (eq? (n prms) (n params)))]
                 [?x false]))
             (fmap-for (fnsyms prm)
               (case (cadr prm)
                 [(?nm . ?prms)
                  `(,(sym.. nm '/map) ,@(map (lambda (x)
                                               (assoc fnsyms (symbol->string x)))
                                             prms)
                                      ,(car prm))]
                 [?x (let* [(xstr (symbol->string x))]
                       (if (= (str/lower (char-at xstr 1)) (char-at xstr 1))
                         `(,(assoc fnsyms (symbol->string x)) ,(car prm))
                         (car prm)))]))
             (make-functor ()
               (let* [(fnsyms (map (lambda (x)
                                     (list (symbol->string x)
                                           (gensym (symbol->string (sym.. x '.function)))))
                                   params))
                      (xsym (gensym))
                      (arguments (inj)
                        (let* [(proc (cdr inj))]
                          (loop [(o '())
                                 (i 1)]
                                [(> i (n proc)) o]
                                (let* [(cur (nth proc i))]
                                  (recur (snoc o
                                               (list
                                                 (if (list? cur)
                                                   (sym.. (car cur) (string->symbol (tostring i)))
                                                   (sym.. cur (string->symbol (tostring i))))
                                                 (nth proc i)))
                                         (succ i))))))
                      (make-case (inj)
                        (if (list? inj)
                          (let* [(args (arguments inj))]
                            `((,(car inj) ,@(map (lambda (x) (sym.. '? (car x)))
                                                 args))
                              (list ',(car inj) ,@(map (lambda (x)
                                                         (fmap-for fnsyms x))
                                                       args))))
                          `(,inj ,inj)))]
                 `(defun ,(sym.. name '/map) (,@(map cadr fnsyms) ,xsym)
                    (case ,xsym
                      ,@(map make-case injections)))))]
      (map (lambda (x)
             (if (list? x)
               (map (lambda (p)
                      (if (! (valid-param? p))
                        (error! (.. (pretty p) " is not a valid parameter") 1)
                        true))
                    (cdr x))
               true))
           injections)
      (unpack (snoc (flatten (list
                               (map make-injection-for injections)
                               (flat-map make-projections-for injections)
                               (map make-coinjection-for injections)))
                    (make-functor))))))

(defun make-projections-for (inj) :hidden
  (cond
    [(list? inj)
     (map (lambda (x)
            (let* [(val (gensym))
                   (f (gensym))]
              `(define ,(sym.. (car inj) '. (string->symbol (tostring x)))
                 (lens (lambda (,val)
                         (if (eq? (car ,val) ,(car inj))
                           (view (at ,(+ x 1)) ,val)
                           ,val))
                       (lambda (,f ,val)
                         (if (eq? (car ,val) ,(car inj))
                           (over (at ,(+ x 1)) ,f ,val)
                           ,val))))))
          (range 1 (n (cdr inj))))]
    [true '()]))

(defun make-coinjection-for (inj) :hidden
  (cond
    [(list? inj)
     (let* [(val (gensym))
            (rest (gensym))
            (fn (gensym))]
       `(define ,(sym.. '_ (car inj))
          (lens (lambda (,val)
                  (case ,val
                    [(,(car inj) ,'. ,(sym.. '? rest))
                     ,rest]
                    [,'_ nil]))
                (lambda (,fn ,val)
                  (case ,val
                    [(,(car inj) ,'. ,(sym.. '? rest))
                     (list ',(car inj) (,fn ,rest))]
                    [,(sym.. '? rest) ,rest])))))]
    [true '()]))

(defun make-injection-for (inj) :hidden
  (cond
    [(list? inj)
     (let* [(out (gensym))
            (inps (gensym))]
       `(defun ,(car inj) (,(sym.. '& inps))
          (if (= (n ,inps) ,(n (cdr inj)))
            (cons ',(car inj) ,inps)
            (error! (.. ,(.. "expected " (n (cdr inj)) " argument(s) for " (symbol->string (car inj)) ", got ")
                        (n ,inps))))))]
    [true
      `(define ,inj ',inj)]))
