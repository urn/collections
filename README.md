Collections
===========

A library containing useful collections for use in Urn code, such as

- Lazy sequences
- Linked lists
- Bidirectional maps

Extensibility
=============

Writing your own collections is not only allowed, but encouraged. If
`collections`, for some reason, does not cover your use case, you can
use the functions provided by `collections` if your collection
implements the following protocol:

- Mapping
  Provide a function `collections/map` in a metatable.
  This function shall receive, in the following order:
  - The function
  - The instance of your collection.

- `car`
  Provide a function `collections/car` in a metatable.
  This function shall receive, in the following order:
  - The instance of your collection

- `cdr`
  Provide a function `collections/cdr` in a metatable.
  This function shall receive, in the following order:
  - The instance of your collection
